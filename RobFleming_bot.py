#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 13 14:02:19 2023

Example code that finds recent tweets that mention 'staff grade'
or 'associate specialist' and replies with Dr Rob Fleming's tweet.'

@author: MSFabus
"""

import tweepy
import time

# Enter your Twitter API keys and access tokens
consumer_key = "your_consumer_key"
consumer_secret = "your_consumer_secret"
access_token = "your_access_token"
access_token_secret = "your_access_token_secret"

# Authenticate with Twitter API
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

# Define the search queries
search_queries = ["staff grade", "associate specialist"]

# Define the reply message
example_tweet_url = "https://twitter.com/RobJimFleming/status/1541013465019604995"
reply_message = f"These terms are no longer in use. For info about SAS careers, see: {example_tweet_url}"

# Keep track of tweets already replied to
replied_tweets = []

# Keep the program running indefinitely
while True:
    try:
        # Search for latest tweets containing the search queries
        for query in search_queries:
            tweets = api.search(q=query, lang="en", result_type="recent")

            # Loop through the tweets
            for tweet in tweets:
                # Ignore any tweets that are replies or retweets
                if tweet.in_reply_to_status_id is not None or tweet.retweeted_status is not None:
                    continue
                
                # Check if tweet has already been replied to
                if tweet.id in replied_tweets:
                    continue
                
                # Reply to the tweet with the reply message
                api.update_status(
                    status=reply_message,
                    in_reply_to_status_id=tweet.id,
                    auto_populate_reply_metadata=True
                )

                # Print confirmation message
                print(f"Replied to tweet by {tweet.user.name}")
                    
                # Add tweet ID to list of replied tweets
                replied_tweets.append(tweet.id)
                
                # Wait for a few seconds before replying to the next tweet
                time.sleep(5)

        # Wait for a minute before searching for the next tweets
        time.sleep(30)

    except tweepy.TweepError as error:
        # Print error message
        print(f"Error: {error}"