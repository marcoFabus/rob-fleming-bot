# Rob Fleming bot

Tired of Twitter users misunderstanding SAS careers or devaluing their existence? No longer!

This repository contains example Python code that finds recent tweets that mention 'staff grade' or 'associate specialist' and replies with this tweet from Dr Rob Fleming: https://twitter.com/RobJimFleming/status/1541013465019604995.'

To get the bot working, you will need to apply for a Twitter development account, and then update the initial four key / access token lines with those of your bot. For a tutorial, see e.g. https://realpython.com/twitter-bot-python-tweepy/. 

Please consult Dr Rob Fleming if you actually want to make this into a Rob Fleming bot. I do not condone any impersonation or misuse of this code.
